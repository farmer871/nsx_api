# VMware NSX-T Data Center 參考說明文件
---

### 產品說明

- [NSX-T Data Center 參考設計指南](https://communities.vmware.com/docs/DOC-37591)
  - [NSX-T Reference Design Guide Version 2.0.pdf](https://communities.vmware.com/servlet/JiveServlet/download/37591-9-228862/NSX-T%20Reference%20Design%20Guide%20Version%202.0.pdf)

- NSX-T Data Center 產品頁面
  - [英文](https://www.vmware.com/products/nsx.html "")
  - [中文](https://www.vmware.com/tw/products/nsx.html "")

- [NSX-T Data Center 部落格](https://blogs.vmware.com/networkvirtualization/ "NSX Blog")

- [VMware Taiwan@Facebook](https://www.facebook.com/vmwaretaiwan/notes/?ref=page_internal)

- [NSX-T Data Center 線上實習實驗室 (HOL)](https://labs.hol.vmware.com/HOL/catalogs/catalog/877 "Hands-On Lab")

- [NSX-T Data Center 訓練和示範影片](https://www.youtube.com/playlist?list=PLdYldEmmLm2nBcfxkp-wzE4SCohw2ynD3 "")

- [NSX-T Data Center 2.5.x 產品功能及授權(KB#76754)](https://kb.vmware.com/s/article/76754 "KB#76754")

- NSX-T 3.0 版本釋出說明
  - [英文](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/3.0/rn/VMware-NSX-T-Data-Center-30-Release-Notes.html)
  - [中文](https://docs.vmware.com/tw/VMware-NSX-T-Data-Center/3.0/rn/VMware-NSX-T-Data-Center-30-Release-Notes.html "")

---

### 快速指南(含操作步驟)

- [vSphere with K8s 與 NSX-T 架構初探](./get_vsphere_with_k8s_and_nsx-t.md)

- [使用NSX-T API 功能初探](./get_nsx-t_via_api.md)

---

### 內文感知微分段(Context-aware Micro-segmenation)

- [Context-aware Micro-segmentation with NSX-T 2.4](https://blogs.vmware.com/networkvirtualization/2019/03/context-aware-micro-segmentation-with-nsx-t-2-4.html/ "VMware Blog")

- [Context Aware Micro Segmentation with NSX Data Center (Youtube)](https://www.youtube.com/watch?v=21FvtORJIes "Video")

- [NSX-T 2.4 Context Aware Firewall Demo (Youtube)](https://www.youtube.com/watch?v=UpRIsJEUTKA "Video")

---

### NSX API

- [NSX API說明文件(本地版)](https://{NSX-Manager-Ip}/policy/api.html "https://{NSX-Manager-Ip}/policy/api.html")

- [How to Navigate NSX-T Policy APIs for Network Automation](https://blogs.vmware.com/networkvirtualization/2020/06/navigating-nsxt-policy-apis.html/ "")

- [Getting Started With NSX-V REST API](http://www.vstellar.com/2018/06/21/getting-started-with-nsx-rest-api/)

---

### 自動化部署及管理

- Terraform
  - [NSX Terraform Provider](https://registry.terraform.io/providers/vmware/nsxt/latest/docs "Terraform")
  - [VMware Blog: NSX-T Automation with Terraform](https://blogs.vmware.com/networkvirtualization/2018/04/nsx-t-automation-with-terraform.html/)
  -[Youtube:NSX-T Infrastructure as a Code using Terraform](https://www.youtube.com/watch?v=3wntKnCq7aQ)

- Ansible
  - [VMware Blog: NSX-T Infrastructure Deployment Using Ansible](https://blogs.vmware.com/networkvirtualization/2019/06/ansible-nsx-t-deployment.html/)
  - [VMware@Github: Ansible for NSX-T](https://github.com/vmware/ansible-for-nsxt)

---

[[回到主頁]](./READMD.md "HOME")
