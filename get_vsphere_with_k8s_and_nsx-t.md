# [快速指南] vSphere with K8s 與 NSX-T 架構初探

## [目的]
透過NSX-T管理界面與Kubectl相互操作，了解大致vSphere with K8s架構已建立K8s叢集的網路狀態。

## [任務]

- 連線vCenter Server檢視vSphere with K8s
- 連線NSX-T管理界面檢視容器與網路資訊

> 連線**vCenter Server**及**NSX-T**應屬於**基礎架構團隊**之任務。

- 使用Kubectl + vSphere Plugin檢視K8s資訊

> 使用**Kubectl管理K8s叢集**應屬於**研究開發團隊**之任務。

## [執行步驟]

1. 連線vCenter Server Web Client(html 5)，點選**功能表** > **工作負載管理**。

 ![001.png](./pics/k8s/001.png)

2.選擇其中一個命名空間(Namespace)：**tzdemo**。

 ![002.png](./pics/k8s/002.png)

3.在**摘要**功能頁可檢視此命名空間摘要資訊。

 ![003.png](./pics/k8s/003.png)

4.點選**運算** > **網繭(Pod)**。從vCenter Web Client可知該命名空間具有3組網繭(Pods)。

 ![004.png](./pics/k8s/004.png)

5.讓我們先透過vSphere適用的Kubernetes CLI工具，使用命令方式連線至**主管叢集(Supervisor Cluster)**。相關資訊請參考[[說明文件]](https://docs.vmware.com/tw/VMware-vSphere/7.0/vmware-vsphere-with-kubernetes/GUID-63A1C273-DC75-420B-B7FD-47CB25A50A2C.html "向主管叢集驗證並取得內容")，在此就不贅述。

 > 已啟用 **vSphere with Kubernetes** 的叢集稱為**主管叢集**。叢集是 **vSphere with Kubernetes** 的基礎，提供執行 **vSphere網繭** 和 **Tanzu Kubernetes叢集**所需的元件和資源。

 > 此任務為研發團隊操作，應視為一般研發團隊日常維運工作

 ![005.png](./pics/k8s/005.png)

 透過命令 `kubectl vsphere login` 連線主管叢集，但原有命令需要手動輸入使用者密碼。所以就簡單寫個程式 `vsphereK8sLogin.sh`，自動將**K8s Control Plane**的IP及連線資訊帶入並建立連線。

 ![006.png](./pics/k8s/006.png)

6.成功連線**主管叢集**後，使用 `kubectl config use-context` 切換至不同內容(context)組態。這裡切換至**tzdemo**。

 ```bash
   $ kubectl config use-context tzdemo
   > Switched to context "tzdemo".
 ```

7.讓我們使用 `kubectl get pods` 取得Pods資訊。應與從vCenter Web Client上取得的資訊一致。共有3組網繭。

 ```bash
   $ kubectl get pods -o wide

   NAME                         READY   STATUS    RESTARTS   AGE    IP             NODE               NOMINATED NODE   READINESS GATES
   hello-k8s-675fffc7dc-84xkv   1/1     Running   0          4d2h   10.244.0.213   esxi2.khdc.local   <none>           <none>
  hello-k8s-675fffc7dc-c9wz7   1/1     Running   0          4d2h   10.244.0.215   esxi1.khdc.local   <none>           <none>
  hello-k8s-675fffc7dc-dl8h4   1/1     Running   0          4d2h   10.244.0.214   esxi2.khdc.local   <none>           <none>
 ```

8.登入NSX-T管理界面。點選**詳細目錄** > **容器** > **命名空間**，使用**篩選工具**選擇**命名空間**：tzdemo。

 ![007.png](./pics/k8s/007.png)

9.點選網繭 `3` 連結以開啟NSX-T中**網繭**資訊。也與 `vCenter Web Clinet` 與 `kubectl` 取得資訊一致。

 ![008.png](./pics/k8s/008.png)

10.接著我們在NSX-T中，點選**網路** > **負載平衡**，使用**篩選工具**選擇**負載平衡器** > **名稱**: tzdemo。

 ![009.png](./pics/k8s/009.png)

11.再次以研發團隊的角度取得K8s使用的負載平衡器資訊。使用 `kubectl describe loadbalcancer` 命令，資訊與在NSX-T管理界面中一致。

 > 研發團隊只要專注於原有K8s叢集的部署，至於網路組態部份，**NSX-T**與**vSphere with K8s**便會自動完成網路相關組態。

 > 對於**研發團隊**可降低佈建K8s叢集網路的複雜繁瑣度。

 > 對於**基礎團隊**可提昇對於K8s叢集網路的管理可視性。

 ```bash
  $ kubectl describe loadbalancer
    Name:         tzdemo-demo-gc-lb
    Namespace:    tzdemo
    Labels:       <none>
    Annotations:  <none>
    API Version:  vmware.com/v1alpha1
    Kind:         LoadBalancer
    Metadata:
      Creation Timestamp:  2020-08-28T03:38:21Z
      Generation:          1
      Owner References:
        API Version:           vmoperator.vmware.com/v1alpha1
        Block Owner Deletion:  true
        Controller:            false
        Kind:                  VirtualMachineService
        Name:                  demo-gc-control-plane-service
        UID:                   0589a8d4-7c91-4289-8a0d-ac4c5c5a0fe2
      Resource Version:        50830853
      Self Link:               /apis/vmware.com/v1alpha1/namespaces/tzdemo/loadbalancers/tzdemo-demo-gc-lb
      UID:                     afdc7139-2cfa-4d6e-8c28-be53b8844038
    Spec:
      Size:                  SMALL
      Virtual Network Name:  demo-gc-vnet
    Status:
    Events:  <none>
 ```

12.在NSX-T管理界面中，點選**網路** > **區段(Segment)**，使用**篩選工具**選擇**區段** > **名稱**: tzdemo。

 可以從拓撲圖看出**區段(Segment)**的連接架構。
  > 以下連接**虛擬機器**與**網繭**。

  > 以上連接**第1層閘道(T1 Gateway)**。

 ![010.png](./pics/k8s/010.png)

  > 研發團隊只要專注於原有K8s叢集的部署，至於網路組態部份，**NSX-T**與**vSphere with K8s**便會自動完成網路相關組態。(**重點**)

## [深入淺出]

1.取得網繭資訊

 > 研發團隊

 使用 `kubectl get pod/<pod_name> -o yaml` 取得 <pod_name> 的YAML檔案。

 ```bash
  $ kubectl get pod/hello-k8s-675fffc7dc-84xkv -o yaml
    apiVersion: v1
    kind: Pod
    metadata:
      annotations:
        kubernetes.io/psp: wcp-default-psp
        mac: 04:50:56:00:d8:10
        vlan: None
        vmware-system-ephemeral-disk-uuid: 6000C29a-1cca-9818-a09e-2d0703231ed8
        vmware-system-image-references: '{"hello-kubernetes":"tanzu-1faabe55db639783609f29b4ae201ba2a5b4fa32-v0"}'
        vmware-system-vm-moid: vm-8280:deac47d3-1258-4579-b541-fd214c22c996
        vmware-system-vm-uuid: 503e8573-c985-a91b-34ad-0fd1516729a5
      creationTimestamp: "2020-08-28T03:53:18Z"
      finalizers:
      - lifecycle-controller/system.vmware.com
      generateName: hello-k8s-675fffc7dc-
      labels:
        app: hello-k8s
        pod-template-hash: 675fffc7dc
      name: hello-k8s-675fffc7dc-84xkv
      namespace: tzdemo
      ownerReferences:
      - apiVersion: apps/v1
        blockOwnerDeletion: true
        controller: true
        kind: ReplicaSet
        name: hello-k8s-675fffc7dc
        uid: 225ade98-95b3-4e10-8cd1-d7ddd909b605
      resourceVersion: "50837067"
      selfLink: /api/v1/namespaces/tzdemo/pods/hello-k8s-675fffc7dc-84xkv
      uid: 9bbf5561-fbe0-4acc-a680-8fdb76e5a3b6
    spec:
      containers:
      - image: 10.7.162.130/tzdemo/tanzu:1.1
        imagePullPolicy: IfNotPresent
        name: hello-kubernetes
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
          name: default-token-bpbpl
          readOnly: true
      dnsPolicy: ClusterFirst
      enableServiceLinks: true
      imagePullSecrets:
      - name: tzdemo-default-image-pull-secret
      nodeName: esxi2.khdc.local
      priority: 0
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      serviceAccount: default
      serviceAccountName: default
      terminationGracePeriodSeconds: 30
      tolerations:
      - effect: NoExecute
        key: node.kubernetes.io/not-ready
        operator: Exists
        tolerationSeconds: 300
      - effect: NoExecute
        key: node.kubernetes.io/unreachable
        operator: Exists
        tolerationSeconds: 300
      volumes:
      - name: default-token-bpbpl
        secret:
          defaultMode: 420
          secretName: default-token-bpbpl
    status:
      conditions:
      - lastProbeTime: null
        lastTransitionTime: "2020-08-28T03:53:18Z"
        status: "True"
        type: PodScheduled
      - lastProbeTime: null
        lastTransitionTime: "2020-08-28T03:53:35Z"
        status: "True"
        type: Initialized
      - lastProbeTime: null
        lastTransitionTime: "2020-08-28T03:53:35Z"
        status: "True"
        type: ContainersReady
      - lastProbeTime: null
        lastTransitionTime: "2020-08-28T03:53:35Z"
        status: "True"
        type: Ready
      containerStatuses:
      - containerID: c0aeb76c-c9c8-41ad-8c59-5575e4e20cf6
        image: ""
        imageID: ""
        lastState: {}
        name: hello-kubernetes
        ready: true
        restartCount: 0
        state:
          running:
            startedAt: "2020-08-28T03:53:32Z"
      hostIP: 10.7.160.12
      phase: Running
      podIP: 10.244.0.213
      podIPs:
      - ip: 10.244.0.213
      qosClass: BestEffort
      startTime: "2020-08-28T03:53:32Z"
 ```

 > 基礎團隊

 選擇其中一個**網繭**，點選**檢視YAML**。
 ![011.png](./pics/k8s/011.png)

 研發團隊使用 `kubectl` 取得資訊應與vCenter Web Client上取得的資訊一致。

 ![012.png](./pics/k8s/012.png)

2.取得部署資訊

 > 研發團隊

 ```bash
  $ kubectl get deployment -o wide
    NAME        READY   UP-TO-DATE   AVAILABLE   AGE    CONTAINERS         IMAGES                          SELECTOR
    hello-k8s   3/3     3            3           4d4h   hello-kubernetes   10.7.162.130/tzdemo/tanzu:1.1   app=hello-k8s
 ```

 > 基礎團隊

 ![013.png](./pics/k8s/013.png)

3.取得複本集資訊

 > 研發團隊

 ```bash
    $ kubectl get replicaset -o wide
    NAME                   DESIRED   CURRENT   READY   AGE    CONTAINERS         IMAGES                            SELECTOR
    hello-k8s-5f898bf846   0         0         0       4d5h   hello-kubernetes   paulbouwer/hello-kubernetes:1.8   app=hello-k8s,pod-template-hash=5f898bf846
    hello-k8s-675fffc7dc   3         3         3       4d4h   hello-kubernetes   10.7.162.130/tzdemo/tanzu:1.1     app=hello-k8s,pod-template-hash=675fffc7dc
 ```

 > 基礎團隊

 ![014.png](./pics/k8s/014.png)


 > 其他部份就讓大家自己嘗試看看囉。

---

- [vSphere with Kubernetes 組態和管理](https://docs.vmware.com/tw/VMware-vSphere/7.0/vmware-vsphere-with-kubernetes/GUID-152BE7D2-E227-4DAA-B527-557B564D9718.html "VMware官方文件")

- [vSphere with Kubernetes基礎](https://docs.vmware.com/tw/VMware-vSphere/7.0/vmware-vsphere-with-kubernetes/GUID-C163490C-BE03-4DFE-8A03-5316D3245765.html "")

- [vSphere with Kubernetes 元件和架構](https://docs.vmware.com/tw/VMware-vSphere/7.0/vmware-vsphere-with-kubernetes/GUID-74EC2571-4352-4E15-838E-5F56C8C68D15.html "")

---
[[回到主頁]](./README.md "HOME")
