# [快速指南] 使用 NSX-T API 功能初探

## [目的]

使用 NSX-T Data Center REST API 功能查詢 NSX-T 組態資訊

## [任務]

- 撰寫簡易API查詢程式
- 查詢計算管理程式(Compute Manager)
- 查詢授權使用狀態(License)
- 查詢分散式防火牆原則(Policy)
- 查詢分散式防火牆規則(Rule)

> NSX-T 提供 **REST API** 功能，可使用熟悉的工具方式與 **NSX-T Manager** 進行聯繫。

> 以下步驟執行平臺爲 **Linux** ， 採用工具爲 **CURL** 和 **BASH**。

> 至於 REST API 使用方式並非此演示重點，請至以下列出 **參考網站** 進行學習。若有問題可與指南作者提問討論。

## [執行步驟]

1.有關 **NSX-T Data Center REST API** 相關資料有 2 個地方可以取得。請選擇方便的方式來獲取相關資訊。

 - [ Internet: VMware {code} - NSX-T 3.0 API ](https://code.vmware.com/apis/976/nsx-t "NSX-T 3.0 API")

 ![001.png](./pics/api/001.png)

 - Local Manager: **https://\<NSX-T_Manager_IP\>/policy/api.html**

 ![002.png](./pics/api/002.png)


2.根據**官方說明文件-Overview**章節內容的說明，與一般使用**REST API**的方式相同，所以基本上我們使用以下CURL命令架構與NSX-T Manager溝通。

 ```bash
  $ curl -k -X <REQUEST_METHOD> -u <USERNAME>:<PASSWORD> https://MANAGER/<API_URI_PATH>
 ```
  - **REQUEST_METHOD**: API指定要求方式。這裏測試的API爲**查詢**動作，一般以**GET**為主。
  - **USERNAME:PASSWORD**: NSX-T驗證使用者帳密資訊。
  - **MANAGER**: NSX-T IP或主機名稱。
  - **API_URI_PATH**: REST API查詢URI位址。請根據需求查詢文件取得。

  > **URL**與**URI**一般也不太區別，所以後續兩者就相互通用了吧！


3.根據上述說明，我們先以取得**計算管理程式(Compute Managers)**資訊爲先例。

 官方文件指出REST API取得方式爲 `GET /api/v1/fabric/compute-managers`。

 - REQUEST_METHOD: **GET**
 - API_URI_PATH: **/api/v1/fabric/compute-managers**

 ![003.png](./pics/api/003.png)

 **API使用參數**：

| Request Key       | Content                              |
| ----------------- | ------------------------------------ |
| Method:           | GET                                  |
| URI Path(s):      | /api/v1/fabric/compute-managers      |


 執行以下**curl命令**。

 ```bash
  $ curl -k -X GET -u admin:password https://192.168.160.51/api/v1/fabric/compute-managers

  {
  "results" : [ {
    "server" : "vc.khdc.local",
    "origin_type" : "vCenter",
    "credential" : {
      "thumbprint" : "F2:58:05:63::81:9E:90:FA",
      "credential_type" : "UsernamePasswordLoginCredential"
    },
    "origin_properties" : [ {
      "key" : "fullName",
      "value" : "VMware vCenter Server 7.0.0 build-16386335"
    }, {
      "key" : "localeVersion",
      "value" : "INTL"
    }, {
      "key" : "version",
      "value" : "7.0.0"
    }, {
      "key" : "originComputeManagerDescription",
      "value" : ""
    }, {
      "key" : "apiVersion",
      "value" : "7.0.0.0"
    }, {
      "key" : "build",
      "value" : "16386335"
    }, {
      "key" : "vendor",
      "value" : "VMware, Inc."
    }, {
      "key" : "licenseProductName",
      "value" : "VMware VirtualCenter Server"
    }, {
      "key" : "name",
      "value" : "VMware vCenter Server"
    }, {
      "key" : "osType",
      "value" : "linux-x64"
    }, {
      "key" : "instanceUuid",
      "value" : "deac47d3-1258-4579-b541-fd214c22c996"
    }, {
      "key" : "originComputeManagerName",
      "value" : "VMware vCenter Server"
    }, {
      "key" : "localeBuild",
      "value" : "000"
    }, {
      "key" : "licenseProductVersion",
      "value" : "7.0"
    }, {
      "key" : "apiType",
      "value" : "VirtualCenter"
    }, {
      "key" : "productLineId",
      "value" : "vpx"
    } ],
    "set_as_oidc_provider" : true,
    "reverse_proxy_https_port" : 443,
    "resource_type" : "ComputeManager",
    "id" : "f1c4e5e8-93b9-48bc-b00a-edf18b9775cd",
    "display_name" : "vc.khdc.local",
    "description" : "",
    "_create_user" : "admin",
    "_create_time" : 1590462680831,
    "_last_modified_user" : "admin",
    "_last_modified_time" : 1590462680831,
    "_protection" : "NOT_PROTECTED",
    "_revision" : 0
  } ],
  "result_count" : 1,
  "sort_by" : "display_name",
  "sort_ascending" : true
 ```

 好的！到目前為止，看來**與NSX-T Manager進行REST API溝通已經成功**了！

 連線到NSX-T管理界面，點選**系統(System)** > **網狀架構(Fabric)** > **計算管理程式(Compute Managers)**，顯示的內容與我們使用API方式查詢的內容應該是一致性的。

 ![004.png](./pics/api/004.png)

 > 透過以上API的查詢測試，我們可以了解到透過這種方式，便可使用**程式化查詢、修改NSX-T相關組態**，進而延伸至**自動化管理組態**，提昇在資料中心維運的效率。


4.讓我們對於API查詢的結果，稍微深入一點點。有發現到回傳的資料形態爲**JSON格式**？！

 根據NSX-T管理界面提供的摘要內容。

 ![005.png](./pics/api/005.png)

 可以與API查詢內容對比一下。


 | NSX-T管理界面 | API回應值 |
 | --- | --- |
 | 名稱 | results[].display_name |
 | 識別碼 | results[].id |
 | 說明 | results[].description |
 | 類型 | results[].origin_type |
 | FQDN或IP位址 | results[].server |
 | 反向Proxy的HTTPS連接埠 | results[].reverse_proxy_https_port |
 | 版本 | results[].origin_properties[] | select(.key=="version") |
 | 啟用信任 | results[].set_as_oidc_provider |


 至於**連線狀態**，需要其他的API進行查詢。

  `GET /api/v1/fabric/compute-managers/<compute-manager-id>/status`

 ```bash
  $ curl -k -X GET -u admin:P@ssw0rd https://192.168.160.51/api/v1/fabric/compute-managers/f1c4e5e8-93b9-48bc-b00a-edf18b9775cd/status

  {
    "last_sync_time": 1598985038492,
    "version": "7.0.0",
    "connection_status": "UP",
    "registration_status": "REGISTERED",
    "registration_errors": [],
    "connection_errors": [],
    "oidc_end_point_id": "df6f43e2c5e3086185eb48b275b610f9a75af130a828137e0090da4a86d87662"
  }
 ```

 進行到這裡，相信對於NSX-T API可以怎麼應用，應該有點**感覺**了！

 > 至於JSON格式的應用，就真的不會在這討論了！


5.根據以上成功的經驗，我們可以撰寫一個程式，達成以下目標。

 - 將NSX-T Manager資訊直接導入。
 - 將NSX-T許可的使用者帳密資訊也直接導入。

 > 將NSX-T Manager及使用者帳密資訊儲存於檔案中，分別以 : 隔開。

 ```
  NSX-T_MANAGER_IP:USERNAME:PASSWORD
 ```

 - 只要將**REQUEST_METHOD**與**API_URI_PATH**作為變數帶入。
 - 將輸出形態以JSON形式

  程式架構如下(可參考[範例程式 nsxApi.sh](./codes/nsxApi.sh "nsxApi.sh"))：

 ```bash
 #!/bin/bash

  passport='nsx.passport'
  nsxMgr=$(cat ${passport} | cut -d: -f1)
  nsxUser=$(cat ${passport} | cut -d: -f2)
  nsxPass=$(cat ${passport} | cut -d: -f3)

  loginCred="${nsxUser}:${nsxPass}"

  apiRequest="$1"
  apiUrl="$2"

  curl -k -s -X ${apiRequest} -H "content: application/json" -u ${loginCred} https://${nsxMgr}${apiUrl} | jq .
 ```

 接著測試一下撰寫的程式。

 ```bash
  $ ./nsxApi.sh GET /api/v1/fabric/compute-managers

  {
    "results": [
      {
        "server": "vc.khdc.local",
        "origin_type": "vCenter",
        "credential": {
          "thumbprint": "F2:58:05:63:F7:C4:5F:E8:7C:E2:D8:18:23:35:A4:01:0E:0D:79:D2:41:97:2F:6B:93:1C:DD:28:81:9E:90:FA",
          "credential_type": "UsernamePasswordLoginCredential"
        },
        "origin_properties": [
          {
            "key": "fullName",
            "value": "VMware vCenter Server 7.0.0 build-16386335"
          },

    <以下省略>
 ```

 到此為止應該是沒問題了！讓我們再試一些其他API吧。


6.列出**傳輸區域(Transport Zones)**

  ![006.png](./pics/api/006.png)


 **API使用參數**：

  | Request Key       | Content                              |
  | ----------------- | ------------------------------------ |
  | Method:           | GET                                  |
  | URI Path(s):      | /api/v1/transport-zones              |

 底下進階使用[jq](https://stedolan.github.io/jq/manual/)(命令列JSON處理工具)，直接將所需要的數值取出。

 ```bash
  $ ./nsxApi.sh GET /api/v1/transport-zones | jq '.results[] | .display_name+", "+.id+", "+.transport_type'

  "Test-VLAN, 6ba60e25-3a34-403a-a0d8-7a57e45edfd8, VLAN"
  "nsx-overlay-transportzone, 1b3a2f36-bfd1-443e-a0f6-4de01abc963e, OVERLAY"
  "nsx-vlan-transportzone, a95c914d-748d-497c-94ab-10d4647daeba, VLAN"
 ```

 選擇其中一個**傳輸區域**進行狀態摘要查詢。

 ```bash
  $ ./nsxApi.sh GET /api/v1/transport-zones/6ba60e25-3a34-403a-a0d8-7a57e45edfd8/summary

  {
    "transport_zone_id": "6ba60e25-3a34-403a-a0d8-7a57e45edfd8",
    "num_transport_nodes": 1,
    "num_logical_switches": 0,
    "num_logical_ports": 0,
    "transport_node_members": [
      {
        "transport_node_display_name": "192.168.160.12",
        "transport_node_id": "3e360689-06fb-4485-a713-35bf83693be8",
        "host_switches": [
          {
            "host_switch_id": "50 3e 06 98 91 6d b4 66-cf 07 6e 5a 3f 5e bd 5e",
            "host_switch_name": "VDS-HTEP",
            "host_switch_type": "VDS",
            "host_switch_mode": "STANDARD"
          }
        ],
        "compute_collection_id": "f1c4e5e8-93b9-48bc-b00a-edf18b9775cd:domain-c8"
      }
    ]
  }
 ```


## [深入淺出]

根據上面的經驗及學習，將API使用再調整一下，可以呈現自己輸出的格式。

##### 1.查詢授權

 點選**系統** > **個授權**
 ![007.png](./pics/api/007.png)

 **授權使用率報告**必須使用**匯出**取得。
 ![008.png](./pics/api/008.png)

 ![009.png](./pics/api/009.png)


**範例程式** [getLicense.sh](./codes/getLicense.sh "查詢授權")

**API使用參數**
- System Administration > Settings > Licenses(3.6.4.3)：


| Request Key       | Content                              |
| ----------------- | ------------------------------------ |
| Method:           | GET                                  |
| URI Path(s):      | /policy/api/v1/infra/domain          |


 ```bash
  $ ./getLicense.sh

  [TASK] 顯示NSX-T系統使用授權資訊
  ===
  {
    "results": [
      {
        "license_key": "你們看不到授權碼，是正確的！",
        "is_eval": false,
        "expiry": 0,
        "is_expired": false,
        "description": "NSX Data Center Enterprise Plus",
        "quantity": 16,
        "capacity_type": "CPU"
      },
      {
        "license_key": "你們看不到授權碼，是正確的！",
        "is_eval": false,
        "expiry": 0,
        "is_expired": false,
        "description": "NSX for vShield Endpoint",
        "quantity": 0,
        "capacity_type": "CPU"
      }
    ],
    "result_count": 2
  }

  [TASK] 顯示NSX-T系統授權使用率報告
  ===
  授權功能                                                	      VM      CPU     USER     vCPU     CORE
  --------                                                    	      --      ---     ----     ----     ----
  Switching & Routing                                         	      35        4       35        0       24
  Context Aware Micro-Segmentation - App Identification       	       0        0        0        0        0
  DFW                                                         	      35        4       35        0       24
  Federation                                                  	       0        0        0        0        0
  Service Insertion                                           	       0        0        0        0        0
  Edge Load Balancer                                          	      35        4       35        0       24
  VPN                                                         	       0        0        0        0        0
  Distributed IDS                                             	       0        4        0        0        0
  Identity Firewall                                           	       0        0        0        0        0
  Context Aware Micro-Segmentation - IDFW for RDSH            	       0        0        0        0        0
  Micro-Segmentation Planning (L4)                            	       0        0        0        0        0
  Enhanced Guest Introspection                                	       0        0        0        0        0
```

##### 2.查詢分散式防火牆原則

點選**安全性** > **分散式防火牆** > **所有規則**
 ![010.png](./pics/api/010.png)

**範例程式** [getFirewallPolicy.sh](./codes/getFirewallPolicy.sh "查詢分散式防火牆原則")

 **API使用參數**
 - Policy > Infra > Domains > Domain(3.4.3.4.1)：


 | Request Key       | Content                              |
 | ----------------- | ------------------------------------ |
 | Method:           | GET                                  |
 | URI Path(s):      | /policy/api/v1/infra/domain          |

 - Policy > Security > East West Security > Distributed Firewall > Rules(3.4.7.1.1.2)：


 | Request Key       | Content                              |
 | ----------------- | ------------------------------------ |
 | Method:           | GET                                  |
 | URI Path(s):      | /policy/api/v1/infra/domains/<domain-id>/security-policies |


 ```bash
 $ ./getFirewallPolicy.sh

  ** DEMO - 使用API取得分散式防火牆原則 **

  [TASK] 取得所屬領域(DOMAIN)
            領域名稱
            --------
   1        default
   2        domain-c8:deac47d3-1258-4579-b541-fd214c22c996
  ---
   共 2 組領域

  [TASK] 取得分散式防火牆原則/所屬領域
  # [default]

            原則名稱                                                 原則類別
            --------                                                 --------
   1        default-layer2-section                                   Ethernet
   2        d24cb420-9fe9-11ea-881a-7511ec60e0d9                     Emergency
   3        9bcc7460-9feb-11ea-881a-7511ec60e0d9                     Infrastructure
   4        7ae73620-9fed-11ea-881a-7511ec60e0d9                     Application
   5        default-layer3-section                                   Application
  ---
  # [domain-c8:deac47d3-1258-4579-b541-fd214c22c996]

            原則名稱                                                 原則類別
            --------                                                 --------
   1        hc_domain-c8:deac47d3-1258-4579-b541-fd214c22c996        Environment
   2        np_ddb4ce87-b4b9-4bcb-8700-740fb62f4a79_whitelist        Application
   3        np_f20a74bf-606a-4453-9261-984ee5e70aa0_whitelist        Application
   4        np_f20a74bf-606a-4453-9261-984ee5e70aa0_isolation        Application
   5        np_ddb4ce87-b4b9-4bcb-8700-740fb62f4a79_isolation        Application
   6        ds_domain-c8:deac47d3-1258-4579-b541-fd214c22c996        Application
  ---
   共 11 組原則
 ```

##### 3.查詢分散式防火牆規則

點選**安全性** > **分散式防火牆** > **類別特定規則**，選擇**3-Tier-CRM**原則，將檢視其包含規則。
 ![011.png](./pics/api/011.png)


**範例程式** [getFirewallRule.sh](./codes/getFirewallRule.sh "查詢分散式防火牆規則")

 **API使用參數**
 - Policy > Security > East West Security > Distributed Firewall > Rules(3.4.7.1.1.2)：


| Request Key       | Content                              |
| ----------------- | ------------------------------------ |
| Method:           | GET                                  |
| URI Path(s):      | /policy/api/v1/infra/domains/<domain-id>/security-policies/<security-policy-id>/rules |


 ```bash
  $ ./getFirewallRules.sh

  [TASK] 顯示[3-Tier-CRM]原則防火牆規則
   - 原則識別碼:7ae73620-9fed-11ea-881a-7511ec60e0d9
  ===
  {
    "動作": "ALLOW",
    "規則名稱": "Allow external",
    "識別碼": 1112,
    "來源": [
      "ANY"
    ],
    "目的地": [
      "/infra/domains/default/groups/CRM-WEB-Tier"
    ],
    "服務": [
      "/infra/services/HTTP",
      "/infra/services/HTTPS"
    ],
    "方向": "IN_OUT",
    "紀錄": false,
    "停用": false,
    "建立者": "admin"
  }
  {
    "動作": "ALLOW",
    "規則名稱": "CRM-Web-2-APP",
    "識別碼": 1114,
    "來源": [
      "/infra/domains/default/groups/CRM-WEB-Tier"
    ],
    "目的地": [
      "/infra/domains/default/groups/CRM-APP-Tier"
    ],
    "服務": [
      "/infra/services/HTTP-8443"
    ],
    "方向": "IN_OUT",
    "紀錄": false,
    "停用": false,
    "建立者": "admin"
  }
  {
    "動作": "ALLOW",
    "規則名稱": "CRM-APP-2_DB",
    "識別碼": 1113,
    "來源": [
      "/infra/domains/default/groups/CRM-APP-Tier"
    ],
    "目的地": [
      "/infra/domains/default/groups/CRM-DB"
    ],
    "服務": [
      "/infra/services/MYSQL"
    ],
    "方向": "IN_OUT",
    "紀錄": false,
    "停用": false,
    "建立者": "admin"
  }
  {
    "動作": "ALLOW",
    "規則名稱": "CRM-Management",
    "識別碼": 1115,
    "來源": [
      "/infra/domains/default/groups/CRM-Managers"
    ],
    "目的地": [
      "/infra/domains/default/groups/CRM"
    ],
    "服務": [
      "/infra/services/SSH",
      "/infra/services/ICMP-ALL"
    ],
    "方向": "IN_OUT",
    "紀錄": false,
    "停用": false,
    "建立者": "admin"
  }
  {
    "動作": "ALLOW",
    "規則名稱": "Block Other Access",
    "識別碼": 1116,
    "來源": [
      "ANY"
    ],
    "目的地": [
      "/infra/domains/default/groups/CRM"
    ],
    "服務": [
      "ANY"
    ],
    "方向": "IN_OUT",
    "紀錄": false,
    "停用": false,
    "建立者": "admin"
  }
 ```


##### 4.登錄vCenter Server

**API使用參數**
- System Administration > Configuration > Fabric > Compute Managers(3.6.1.2.3)：


| Request Key       | Content                              |
| ----------------- | ------------------------------------ |
| Method:           | POST                                 |
| URI Path(s):      | /api/v1/fabric/compute-managers      |
| Request Body:     | computeManager                       |

 ```bash
  $ curl -k -u admin:password -X POST -H "Content-type: application/json" -d @vc.json https://192.168.160.51/api/v1/fabric/compute-managers
    {
     "server" : "192.168.150.1",
     "origin_type" : "vCenter",
     "credential" : {
       "thumbprint" : "46:E3:B3:94::9D:1A:90:45",
       "credential_type" : "UsernamePasswordLoginCredential"
     },
     "set_as_oidc_provider" : false,
     "reverse_proxy_https_port" : 443,
     "resource_type" : "ComputeManager",
     "id" : "5d5460b2-27cd-4118-99de-7e94b2c74a34",
     "display_name" : "khlab",
     "description" : "",
     "_create_user" : "admin",
     "_create_time" : 1599019298876,
     "_last_modified_user" : "admin",
     "_last_modified_time" : 1599019298876,
     "_protection" : "NOT_PROTECTED",
     "_revision" : 0
    }
 ```

 `vc.json` 內容

 ```json
   {
    "server": "192.168.150.1",
    "origin_type": "vCenter",
    "display_name": "khlab",
    "set_as_oidc_provider": false,
    "credential" : {
      "credential_type": "UsernamePasswordLoginCredential",
      "username": "administrator@vsphere.local",
      "password": "P@ssw0rd",
      "thumbprint": "46:E3:B3:94::9D:1A:90:45"
     }
    }
 ```

 如何取得vCenter Server Thumbprint的方式。

 ```bash
  $ echo -n | openssl s_client -connect <vCenter_Server> 2>/dev/null | openssl x509 -noout -fingerprint -sha256
 ```

 從NSX-T管理界面可以看到有另一台vCenter Server已完成註冊。

 ![012.png](./pics/api/012.png)

 點選剛註冊的vCenter Serverv概觀資訊。

 ![013.png](./pics/api/013.png)


##### 5.刪除計算管理程式

**API使用參數**
- System Administration > Configuration > Fabric > Compute Managers(3.6.1.2.3)：


| Request Key       | Content                              |
| ----------------- | ------------------------------------ |
| Method:           | DELETE                               |
| URI Path(s):      | /api/v1/fabric/compute-managers/<compute-manager-id> |


```bash
curl -k -u admin:password -X DELETE https://192.168.160.51/api/v1/fabric/compute-managers/5d5460b2-27cd-4118-99de-7e94b2c74a34
```

---

## 參考網站

- [4 Most Used REST API Authentication Methods](https://blog.restcase.com/4-most-used-rest-api-authentication-methods/ "REST API Tips")

- [NSX-T Data Center REST API 3.0](https://code.vmware.com/apis/976/nsx-t "nsx-t 3.0 api")

- [NSX-T Data Center REST API 3.0.1](https://code.vmware.com/apis/1030/nsx-t "nsx-t 3.0.1 api")

- [Extracting SSL Thumbprint from ESXi](https://www.virtuallyghetto.com/2012/04/extracting-ssl-thumbprint-from-esxi.html "William大神")
---
[[回到主頁]](./READMD.md "HOME")
