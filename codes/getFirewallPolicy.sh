#!/bin/bash
#
# edited by semigod
#
# 2020/08/30

getPolicy () {
  domainId=$1
  ./nsxApi.sh GET /policy/api/v1/infra/domains/${domainId}/security-policies \
  | jq -r '.results[] | .id+","+.category' > ${domainId}.policy
}

passport='nsx.passport'
if [ ! -f ${passport} ]; then
    echo -e "<!> NSX Passport Not FOUND<!>"
    echo -e " Please add \"<nsx_ip>:<login_user>:<login_passport>\" to ${passport}\n"
    exit
fi

echo -e "** DEMO - 使用API取得分散式防火牆原則 **"
echo -e "\n[TASK] 取得所屬領域(DOMAIN)"
domain=($(./nsxApi.sh GET /policy/api/v1/infra/domains | jq -r '.results[] | .id'))

printf " %-8s %-20s\n" " " "領域名稱"
printf " %-8s %-20s\n" " " "--------"

for ((i=0;i<${#domain[@]};i++)); do
#  num=$(expr $i + 1)
  printf " %-8s %-20s\n" "$(expr $i + 1)" "${domain[$i]}"
  domainCount=$(expr ${domainCount} + 1)
done

echo -e "---"
echo -e " 共 ${domainCount} 組領域"

##
echo -e "\n[TASK] 取得分散式防火牆原則/所屬領域"

for ((i=0;i<${#domain[@]};i++)); do
  getPolicy ${domain[$i]}
done

for ((i=0;i<${#domain[@]};i++)); do
  policyId=($(cat ${domain[$i]}.policy | cut -d, -f1))
  policyCategory=($(cat ${domain[$i]}.policy | cut -d, -f2))
  #policyCount=${#policyName[@]}
  echo -e "# [${domain[$i]}]\n"
  printf " %-8s %-60s %-20s\n" " " "原則名稱" "原則類別"
  printf " %-8s %-56s %-20s\n" " " "--------" "--------"

  for ((j=0;j<${#policyId[@]};j++)); do
    printf " %-8s %-56s %-20s\n" "$(expr $j + 1)" "${policyId[$j]}" "${policyCategory[$j]}"
    policyCount=$(expr ${policyCount} + 1)
  done

  echo "---"
done

echo -e " 共 ${policyCount} 組原則"
