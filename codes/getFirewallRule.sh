#!/bin/bash
#
# edited by semigod
#
# 2020/08/30

passport='nsx.passport'
if [ ! -f ${passport} ]; then
    echo -e "<!> NSX Passport Not FOUND<!>"
    echo -e " Please add \"<nsx_ip>:<login_user>:<login_passport>\" to ${passport}\n"
    exit
fi

echo -e "[TASK] 顯示[3-Tier-CRM]原則防火牆規則"
echo -e " - 原則識別碼:7ae73620-9fed-11ea-881a-7511ec60e0d9"
echo -e "==="
./nsxApi.sh GET /policy/api/v1/infra/domains/default/security-policies/7ae73620-9fed-11ea-881a-7511ec60e0d9/rules \
| jq '.results[] | {"動作": .action, "規則名稱": .display_name, "識別碼": .rule_id, "來源": .source_groups, "目的地": .destination_groups, "服務": .services, "方向": .direction, "紀錄": .logged, "停用": .disabled, "建立者": ._create_user}'

echo
