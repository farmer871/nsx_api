#!/bin/bash
#
# edited by semigod
#
# 2020/08/30

passport='nsx.passport'
if [ ! -f ${passport} ]; then
    echo -e "<!> NSX Passport Not FOUND<!>"
    echo -e " Please add \"<nsx_ip>:<login_user>:<login_passport>\" to ${passport}\n"
    exit
fi

./nsxApi.sh GET /api/v1/fabric/compute-managers \
| jq '.results[] | {"名稱": .server, "識別碼": .id, "類型": .origin_type}'
