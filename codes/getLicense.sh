#!/bin/bash
#
# edited by semigod
#
# 2020/08/30

passport='nsx.passport'
if [ ! -f ${passport} ]; then
    echo -e "<!> NSX Passport Not FOUND<!>"
    echo -e " Please add \"<nsx_ip>:<login_user>:<login_passport>\" to ${passport}\n"
    exit
fi


echo -e "[TASK] 顯示NSX-T系統使用授權資訊"
echo -e "==="
./nsxApi.sh GET /api/v1/licenses | sed 's/"license_key":\ .*$/"license_key":\ "你們看不到授權碼，是正確的！",/' | jq

echo
##
oldIFS=$IFS
IFS=$'\n'

licenseFeatureName=($(./nsxApi.sh GET /api/v1/licenses/licenses-usage | jq '.feature_usage_info[] | .feature'))
licenseFeatureCount=${#licenseFeatureName[@]}
capacityType=("VM" "CPU" "USER" "vCPU" "CORE")
capacityTypeCount=${#capacityType[@]}

vmUsageCount=($(./nsxApi.sh GET /api/v1/licenses/licenses-usage | jq ".feature_usage_info[].capacity_usage[] | select(.capacity_type==\"VM\") | .usage_count"))
cpuUsageCount=($(./nsxApi.sh GET /api/v1/licenses/licenses-usage | jq ".feature_usage_info[].capacity_usage[] | select(.capacity_type==\"CPU\") | .usage_count"))
userUsageCount=($(./nsxApi.sh GET /api/v1/licenses/licenses-usage | jq ".feature_usage_info[].capacity_usage[] | select(.capacity_type==\"USER\") | .usage_count"))
vcpuUsageCount=($(./nsxApi.sh GET /api/v1/licenses/licenses-usage | jq ".feature_usage_info[].capacity_usage[] | select(.capacity_type==\"vCPU\") | .usage_count"))
coreUsageCount=($(./nsxApi.sh GET /api/v1/licenses/licenses-usage | jq ".feature_usage_info[].capacity_usage[] | select(.capacity_type==\"CORE\") | .usage_count"))

echo -e "[TASK] 顯示NSX-T系統授權使用率報告"
echo -e "==="
printf "%-60s\t%8s %8s %8s %8s %8s\n" "授權功能" "VM" "CPU" "USER" "vCPU" "CORE"
printf "%-60s\t%8s %8s %8s %8s %8s\n" "--------" "--" "---" "----" "----" "----"

for ((i=0;i<${licenseFeatureCount};i++)); do
  printf "%-60s\t%8s %8s %8s %8s %8s\n" "$(echo ${licenseFeatureName[$i]} | sed 's/"//g')" \
                                       "${vmUsageCount[$i]}" \
                                       "${cpuUsageCount[$i]}" \
                                       "${userUsageCount[$i]}" \
                                       "${vcpuUsageCount[$i]}" \
                                       "${coreUsageCount[$i]}"
done

echo
