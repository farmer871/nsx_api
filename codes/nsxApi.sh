#!/bin/bash
#
# edited by semigod
#
# 2020/08/30

passport='nsx.passport'
if [ ! -f ${passport} ]; then
    echo -e "<!> NSX Passport Not FOUND<!>"
    echo -e " Please add \"<nsx_ip>:<login_user>:<login_passport>\" to ${passport}\n"
    exit
fi

nsxMgr=$(cat ${passport} | cut -d: -f1)
nsxUser=$(cat ${passport} | cut -d: -f2)
nsxPass=$(cat ${passport} | cut -d: -f3)

loginCred="${nsxUser}:${nsxPass}"

apiRequest="$1"
apiUrl="$2"
if [ $# -ne 2 ]; then
  echo -e "<?> Usage: $0 <API_Request> <API_URL>\n"
  exit
fi

#baseApiUrl="https://${nsxMgr}"

if [ ! -z $(echo ${apiUrl} | grep yaml) ]; then
  curl -k -s -X ${apiRequest} -u ${loginCred} https://${nsxMgr}${apiUrl}
else
  curl -k -s -X ${apiRequest} -H "content: application/json" -u ${loginCred} https://${nsxMgr}${apiUrl} | jq .
fi
